# 0.1.12

* fix README wrong example tissue tag

# 0.1.11

* case_id is now no longer appended at the local_slide_id (specified by json file, displayed in DMC / WBC)
* refactoring for code quality / maintenance / extendability

# 0.1.10

* bugfix upload-id not written into storage mapper
* bugfix not checking CDS case deleted when IDM case found

# 0.1.9

* bugfix

# 0.1.8

* bugfix concerning duplicate slide-id in different cases (should be allowed)

# 0.1.6 / 0.1.7

* test CI pipeline main branch

# 0.1.5

* fix CI pipeline

# 0.1.4

* updated CI pipeline

# 0.1.3

* updated CI pipeline

# 0.1.2

* test commit

# 0.1.1

* implemented basic usage
* only serial program flow

**TODO:** automated python tests
**TODO:** (opt): parallelize uploads across files (check if file access to tus storage file is ok with that)
