version: "3.8"

x-logging:
    &logging-service
    driver: "json-file"
    options:
        tag: "{{.ImageName}}|{{.Name}}|{{.ImageFullID}}|{{.FullID}}"

services:
    # Frontends
    workbench-client:
        image: registry.gitlab.com/empaia/integration/frontend-workspace/workbench-client:0.7.10@sha256:887ed8d67cf95a85bb8d31cf49ab8bf7aaf4977b54473ab7b1ea0a38f5c358ac
        restart: "no"
        environment:
            WBC_WBS_SERVER_API_URL: http://localhost:10001
            WBC_AUTHENTICATION_ON: 'true'
            WBC_IDP_URL: ${IDP_FRONTEND_URL}
            WBC_CLIENT_ID: ${WBC_CLIENT_ID}
            WBC_IDP_HTTPS_NOT_REQUIRED: 'true'
        ports:
            - 127.0.0.1:10051:80
        logging: *logging-service

    data-management-client:
        image: registry.gitlab.com/empaia/integration/data-management-client:0.8.3@sha256:e19ab473859e5952c4389ec57303940c8162e4bd0ea11eed6fb668cd9e1f6bbe
        restart: "no"
        environment:
            MDS_URL: http://localhost:10006
            US_URL: http://localhost:10005
            IDM_URL: http://localhost:10007
            CLIENT_ID: ${MDC_CLIENT_ID}
            AUTH_URL: ${IDP_FRONTEND_URL}
        ports:
            - 127.0.0.1:10054:80
        logging: *logging-service

    workbench-client-v2:
        image: registry.gitlab.com/empaia/integration/frontend-workspace/workbench-client-v2:0.12.1@sha256:d2a5f461632910d99763143c0497b555e0d550b087ecec9d12ddb65f4d4d2c1f
        restart: "no"
        environment:
            WBC_WBS_SERVER_API_URL: http://localhost:10001
            WBC_AUTHENTICATION_ON: 'true'
            WBC_IDP_URL: ${IDP_FRONTEND_URL}
            WBC_CLIENT_ID: ${WBC_CLIENT_ID}
            WBC_IDP_HTTPS_NOT_REQUIRED: 'true'
        ports:
            - 127.0.0.1:10055:80
        logging: *logging-service

    workbench-client-v3:
        image: registry.gitlab.com/empaia/integration/frontend-workspace/workbench-client-v3:0.3.1@sha256:b7826d05f286036e729d07b13be2f39c871c6eef8a44070f568b23f935abc106
        restart: "no"
        environment:
            WBC_WBS_SERVER_API_URL: http://localhost:10001
            WBC_IDP_HTTPS_NOT_REQUIRED: 'true'
            WBC_AUTHENTICATION_ON: 'true'
            WBC_IDP_URL: ${IDP_FRONTEND_URL}
            WBC_CLIENT_ID: ${WBC_CLIENT_ID}
        ports:
            - 127.0.0.1:20057:80
        logging: *logging-service

    sample-app:
        image: registry.gitlab.com/empaia/integration/frontend-workspace/sample-app:0.6.3@sha256:96e478e8ea5c5e2f81e0f0ca2530aee85c6c42632b8d870fe6337b586437f207
        restart: "no"
        logging: *logging-service

    generic-app-ui-v2:
        image: registry.gitlab.com/empaia/integration/frontend-workspace/generic-app-ui-v2:0.0.5@sha256:fe271ef549b39d38542ae03f12da0fd4d94614a67cd78429e9d2f649ac52c676
        restart: "no"
        logging: *logging-service

    generic-app-ui-v3:
        image: registry.gitlab.com/empaia/integration/frontend-workspace/generic-app-ui-v3:0.2.2@sha256:14742ceb5c529e2df4f3604c3d15d72f1f00fb86df44af9742cc3450c00a120a
        restart: "no"
        logging: *logging-service

    # Services with Ports
    workbench-service:
        image: registry.gitlab.com/empaia/services/workbench-service:0.8.8@sha256:91760a92dfc945e2c57b7d8b47ff38784a24b50e716750f6cb942dd85546b19a
        command: run.sh --workers=4 --host=0.0.0.0 --port=8000
        restart: "no"
        environment:
            WBS_CONNECTION_TIMEOUT: 300
            WBS_CONNECTION_CHUNKSIZE: 1024000
            WBS_AAA_SERVICE_URL: ${AAA_URL}
            WBS_MEDICAL_DATA_SERVICE_URL: http://medical-data-service:5000
            WBS_JOB_EXECUTION_SERVICE_URL: http://job-execution-service:8000
            WBS_MARKETPLACE_SERVICE_URL: ${MPS_URL}
            WBS_ID_MAPPER_URL: http://id-mapper-service:8000
            WBS_DEBUG: 'true'
            WBS_DISABLE_DB_MIGRATIONS: 'false'
            WBS_API_V1_INTEGRATION: workbench_service.api.v1.integrations.empaia:EmpaiaApiIntegration
            WBS_API_V2_INTEGRATION: workbench_service.api.v2.integrations.empaia:EmpaiaApiIntegration
            WBS_API_V3_INTEGRATION: workbench_service.api.v3.integrations.empaia:EmpaiaApiIntegration
            WBS_AUDIENCE: org.empaia.auth.wbs
            WBS_IDP_URL: ${IDP_URL}
            WBS_TOKEN_URL_PATH: /protocol/openid-connect/token
            WBS_CLIENT_ID: ${WBS_CLIENT_ID}
            WBS_CLIENT_SECRET: ${WBS_CLIENT_SECRET}
            WBS_ORGANIZATION_ID: ${ORGANIZATION_ID}
            WBS_CORS_ALLOW_ORIGINS: '["*"]'
            WBS_APP_UI_FRAME_ANCESTORS: "*"
            WBS_OPENAPI_TOKEN_URL: ${WBS_OPENAPI_TOKEN_URL}
            WBS_OPENAPI_AUTH_URL: ${WBS_OPENAPI_AUTH_URL}
            WBS_FRONTEND_CSP_URL: ${WBS_FRONTEND_CSP_URL}
            WBS_GENERIC_APP_UI_V2_URL: http://generic-app-ui-v2
            WBS_GENERIC_APP_UI_V3_URL: http://generic-app-ui-v3
            NO_PROXY: ${COMPOSE_NO_PROXY}
        ports:
            - 127.0.0.1:10001:8000
        volumes:
            - wbs-rsa-vol:/opt/app/bin:rw
        depends_on:
            - keycloak
        logging: *logging-service
        extra_hosts:
            - "host.docker.internal:host-gateway"

    workbench-daemon:
        image: registry.gitlab.com/empaia/services/workbench-service:0.8.8@sha256:91760a92dfc945e2c57b7d8b47ff38784a24b50e716750f6cb942dd85546b19a
        command: wbd
        restart: "no"
        environment:
            WBS_MEDICAL_DATA_SERVICE_URL: http://medical-data-service:5000
            WBS_JOB_EXECUTION_SERVICE_URL: http://job-execution-service:8000
            WBS_APP_SERVICE_URL: http://app-service:8000
            WBS_IDP_URL: ${IDP_URL}
            WBS_CLIENT_ID: ${WBS_CLIENT_ID}
            WBS_CLIENT_SECRET: ${WBS_CLIENT_SECRET}
            WBS_ORGANIZATION_ID: ${ORGANIZATION_ID}
            WBS_DEBUG: "true"
            WBS_MARKETPLACE_SERVICE_URL: ${MPS_URL}
            NO_PROXY: "medical-data-service,job-execution-service,marketplace-service-mock,keycloak"
        depends_on:
            - medical-data-service
        logging: *logging-service

    upload-service:
        image: registry.gitlab.com/empaia/services/loadtus-service:0.1.29@sha256:cc5aef1d60765c8fa4dde0ef84fa98844848f2ce4b676dc9d5dd6b2a52b51641
        restart: "no"
        environment:
            LS_DATA_DIR: /home/appuser/files/
            LS_REQUIRE_FILE_NAME_META_DATA: "True"
            LS_REQUIRE_ID_META_DATA: "True"
            LS_INFO_DIR: /home/appuser/info_files/
            LS_HOST: http://localhost:10005
            LS_ROOT_PATH: /
            LS_CORS_ALLOW_ORIGINS: '["*"]'
            LS_API_INTEGRATION: loadtus_service.api.v1.integrations.empaia:EmpaiaApiIntegration
            LS_IDP_URL: ${IDP_URL}
            LS_AUDIENCE: org.empaia.auth.us
            NO_PROXY: "keycloak"
        ports:
            - 127.0.0.1:10005:8006
        user: root
        volumes:
            - ${PATH_TO_WSIS}:/home/appuser/files
            - us-info:/home/appuser/info_files/:rw
        command:
            - uvicorn
            - loadtus_service.main:app
            - --port=8006
            - --host=0.0.0.0
        logging: *logging-service

    medical-data-service:
        image: registry.gitlab.com/empaia/services/medical-data-service:0.7.7@sha256:d4d11abaa3b080ff31232ff0bb18c205d205ab2e53a8d07ba1f50f16ab2f8329
        restart: "no"
        command:
            - uvicorn
            - --workers=4
            - --host=0.0.0.0
            - --port=5000
            - medical_data_service.app:app
        environment:
            MDS_CONNECTION_CHUNK_SIZE: 1024000
            MDS_CONNECTION_TIMEOUT: 300
            MDS_DEBUG: "True"
            MDS_CORS_ALLOW_ORIGINS: '["*"]'
            MDS_API_INTEGRATION: medical_data_service.api.integrations.empaia:EmpaiaApiIntegration
            MDS_ROOT_PATH: ""
            MDS_DISABLE_OPENAPI: "False"
            MDS_CDS_URL: http://clinical-data-service:8000
            MDS_WS_URL: http://wsi-service:8080
            MDS_SMS_URL: http://storage-mapper-service:8000
            MDS_ES_URL: http://examination-service:8000
            MDS_JS_URL: http://job-service:8000
            MDS_AS_URL: http://annotation-service:8000
            MDS_IDP_URL: ${IDP_URL}
            MDS_TOKEN_URL_PATH: /protocol/openid-connect/token
            MDS_AUDIENCE: org.empaia.auth.mds
            MDS_OPENAPI_TOKEN_URL: ${MDS_OPENAPI_TOKEN_URL}
            NO_PROXY: "clinical-data-service,wsi-service,storage-mapper-service,examination-service,job-service,annotation-service,keycloak"
        ports:
            - 127.0.0.1:10006:5000
        depends_on:
            - clinical-data-service
            - annotation-service
            - examination-service
            - job-service
            - storage-mapper-service
            - wsi-service
            - keycloak
        logging: *logging-service

    id-mapper-service:
        image: registry.gitlab.com/empaia/services/id-mapper-service:0.1.54@sha256:e9bfe26c732ebbe58269ad43f5a046a9381c4ccfe97ff62b25b35508542789fc
        command: run.sh --host=0.0.0.0 --port=8000
        restart: "no"
        environment:
            IDM_DB_USERNAME: empaia_test
            IDM_DB_PASSWORD: A6tP3osxByeM
            IDM_DB: idm
            IDM_DB_HOST: id-mapper-db
            IDM_DB_PORT: 5432
            IDM_DEBUG: "False"
            IDM_DISABLE_DB_MIGRATIONS: "False"
            IDM_ROOT_PATH: /
            IDM_CORS_ALLOW_ORIGINS: '["*"]'
            IDM_API_V1_INTEGRATION: id_mapper_service.api.v1.integrations.empaia:EmpaiaApiIntegration
            IDM_IDP_URL: ${IDP_URL}
            IDM_TOKEN_URL_PATH: /protocol/openid-connect/token
            IDM_AUDIENCE: org.empaia.auth.idms
            IDM_OPENAPI_TOKEN_URL: ${IDM_OPENAPI_TOKEN_URL}
            NO_PROXY: "keycloak"
        ports:
            - 127.0.0.1:10007:8000
        depends_on:
            - id-mapper-db
        logging: *logging-service

    job-execution-service:
        image: registry.gitlab.com/empaia/services/job-execution-service:0.5.23@sha256:2e5bad4c4cdbfc7ef5a2f28d0d4213d6fb4a1459be7e466885d70a85415e2d8a
        depends_on:
            - job-execution-service-db
        environment:
            JES_DATABASE_URL: mongodb://job-execution-service-db:27017/
            JES_ROOT_PATH: /
            JES_DISABLE_OPENAPI: "False"
            JES_DOCKER_NETWORK: auth_default
            JES_MAX_NUMBER_OF_JOBS: 3
            JES_MPS_URL: ${MPS_URL}
            JES_MPS_USE_V1_ROUTES: ${USE_MPS_V1_ROUTES}
            JES_IS_COMPUTE_PROVIDER: ${JES_IS_COMPUTE_PROVIDER}
            # Authentication
            JES_ENABLE_RECEIVER_AUTH: "True"
            JES_IDP_URL: ${IDP_URL}
            JES_TOKEN_URL_PATH: /protocol/openid-connect/token
            JES_AUDIENCE: org.empaia.auth.jes
            JES_CLIENT_ID: ${JES_CLIENT_ID}
            JES_CLIENT_SECRET: ${JES_CLIENT_SECRET}
            JES_OPENAPI_TOKEN_URL: ${JES_OPENAPI_TOKEN_URL}
            NO_PROXY: "job-execution-service-db,keycloak,marketplace-service-mock"
        ports:
            - 127.0.0.1:10008:8000
        volumes:
            - /var/run/docker.sock:/var/run/docker.sock
            # ENABLE when developing behind a proxy
            # - ./jes_docker_config:/root/.docker/
        logging: *logging-service

    app-service:
        image: registry.gitlab.com/empaia/services/app-service:0.7.17@sha256:98bbbd2a0eb6126cc4aedb87d015e43a69d378dd2babde24be3e384f054d48a8
        environment:
            AS_IDP_URL: ${IDP_URL}
            AS_MPS_URL: ${MPS_URL}
            AS_CLIENT_ID: ${AS_CLIENT_ID}
            AS_CLIENT_SECRET: ${AS_CLIENT_SECRET}
            AS_ORGANIZATION_ID: ${ORGANIZATION_ID}
            AS_MDS_URL: http://medical-data-service:5000
            AS_MPS_USE_V1_ROUTES: ${USE_MPS_V1_ROUTES}
            NO_PROXY: "keycloak,marketplace-service-mock,medical-data-service"
            AS_ENABLE_TOKEN_VERIFICATION: "true"
        command:
            - --port=8000
            - --host=0.0.0.0
        ports:
            - 127.0.0.1:10009:8000
        logging: *logging-service

    marketplace-service-mock:
        image: registry.gitlab.com/empaia/service-mocks/marketplace-service-mock:0.1.53@sha256:9780c2a0d2ed15868a3c71cac1b28feffea56a72eb1d989d22a13d9bcd0736b3
        command:
            - uvicorn
            - --workers=1
            - --host=0.0.0.0
            - --port=8000
            - marketplace_service_mock.app:app
        restart: "no"
        environment:
            MPSM_DEBUG: "false"
            MPSM_CORS_ALLOW_CREDENTIALS: "false"
            MPSM_CORS_ALLOW_ORIGINS: '["*"]'
            MPSM_API_INTEGRATION: marketplace_service_mock.api.integrations.disable_auth:DisableAuth
            MPSM_ROOT_PATH: ""
            MPSM_DISABLE_OPENAPI: "false"
            MPSM_DISABLE_API_V0: ${MPSM_DISABLE_API_V0}
            MPSM_ENABLE_API_V1: ${MPSM_ENABLE_API_V1}
        ports:
            - 127.0.0.1:10010:8000
        volumes:
            - mps-mock-vol:/data
        logging: *logging-service

    aaa-service-mock:
        image: registry.gitlab.com/empaia/service-mocks/aaa-service-mock:0.1.37@sha256:49e5cee3996a11a4335ae0fa6487b5e3b2e7156bcc6e8a2a2d9eaa2b98750625
        command:
            - uvicorn
            - --workers=1
            - --host=0.0.0.0
            - --port=8000
            - aaa_service_mock.app:app
        restart: "no"
        environment:
            AAAM_DEBUG: "false"
            AAAM_API_V1_INTEGRATION: aaa_service_mock.api.v1.integrations.disable_auth:DisableAuth
            AAAM_ROOT_PATH: /
            AAAM_DISABLE_OPENAPI: "false"
            AAAM_CORS_ALLOW_CREDENTIALS: "false"
            AAAM_CORS_ALLOW_ORIGINS: '["*"]'
        ports:
            - 127.0.0.1:10011:8000
        volumes:
            - aaa-mock-vol:/data
        logging: *logging-service

    # Services without ports
    wsi-service:
        image: registry.gitlab.com/empaia/services/wsi-service-plugin-integration:0.10.13@sha256:b7f5d2bf90a530aa00646accda1c2435c5a0c4d09c29c9f6f12e66a54449e689
        restart: "no"
        environment:
            WS_DISABLE_OPENAPI: "False"
            WS_ISYNTAX_PORT: 5556
            WS_DEBUG: "False"
            WS_DATA_DIR: /data
            WS_MAPPER_ADDRESS: http://storage-mapper-service:8000/v1/slides/{slide_id}
            WS_LOCAL_MODE: "False"
            WS_MAX_RETURNED_REGION_SIZE: 25000000
            WS_ROOT_PATH: /
            WS_INACTIVE_HISTO_IMAGE_TIMEOUT_SECONDS: 600
            NO_PROXY: "storage-mapper-service"
        depends_on:
            - storage-mapper-service
        volumes:
            - ${PATH_TO_WSIS}:/data
        logging: *logging-service

    job-service:
        image: registry.gitlab.com/empaia/services/job-service:0.7.2@sha256:615c90ce7b353208da06e5deefcebcd74c4705e976123b9b0d2a298505e815e4
        command: run.sh --host=0.0.0.0 --port=8000
        restart: "no"
        environment:
            JS_DB_HOST: mds-db
            JS_DB_PORT: 5432
            JS_DB_USERNAME: empaia_test
            JS_DB_PASSWORD: A6tP3osxByeM
            JS_DB: mds
            PYTHONUNBUFFERED: 1
            JS_ROOT_PATH: /
            JS_RSA_KEYS_DIRECTORY: /app/rsa
        volumes:
            - js-rsa-vol:/app/rsa:rw
        depends_on:
            - mds-db
        logging: *logging-service

    annotation-service:
        image: registry.gitlab.com/empaia/services/annotation-service:0.16.3@sha256:69078a1090259062297aac0f34a6b920034d7f2e16596da1d456f74a543c182f
        restart: "no"
        command:
            - run.sh
            - --host=0.0.0.0
            - --workers=4
            - --port=8000
        environment:
            ANNOT_DB_HOST: mds-db
            ANNOT_DB_PORT: 5432
            ANNOT_DB: mds
            ANNOT_DB_USERNAME: empaia_test
            ANNOT_DB_PASSWORD: A6tP3osxByeM
            ANNOT_ROOT_PATH: /
            ANNOT_API_V1_INTEGRATION: annotation_service.api.v1.integrations.disable_auth:DisableAuth
            ANNOT_API_V3_INTEGRATION: annotation_service.api.v3.integrations.disable_auth:DisableAuth
        depends_on:
            - mds-db
        logging: *logging-service

    clinical-data-service:
        image: registry.gitlab.com/empaia/services/clinical-data-service:0.3.2@sha256:2ab03e71c690697628ef931e0ee77e2910391222e52e1f2b43f4a030edc0d94e
        restart: "no"
        command: run.sh --host=0.0.0.0 --port=8000
        depends_on:
            - mds-db
        environment:
            CDS_DB_HOST: mds-db
            CDS_DB_PORT: 5432
            CDS_DB_USERNAME: empaia_test
            CDS_DB_PASSWORD: A6tP3osxByeM
            CDS_DB: mds
        logging: *logging-service

    examination-service:
        image: registry.gitlab.com/empaia/services/examination-service:0.7.19@sha256:4985e5763c7c5325c6aa43932a993a890f0725737c9115310b20b8e093ab8b1e
        restart: "no"
        command: run.sh --host=0.0.0.0 --port=8000
        environment:
            ES_DB_USERNAME: empaia_test
            ES_DB_PASSWORD: A6tP3osxByeM
            ES_DB: mds
            ES_DB_HOST: mds-db
            ES_DB_PORT: 5432
            ES_API_V1_INTEGRATION: examination_service.api.v1.integrations.disable_auth:DisableAuth
            PYTHONUNBUFFERED: 1
        volumes:
            - es-rsa-vol:/opt/app/bin:rw
        depends_on:
            - mds-db
        logging: *logging-service

    storage-mapper-service:
        image: registry.gitlab.com/empaia/services/storage-mapper-service:0.2.7@sha256:bf64899db74d6ba84ff60ab9d5c9af8ea5be875a7ac9f3900f45eaa2598d7fc9
        restart: "no"
        environment:
            SM_DB_USERNAME: empaia_test
            SM_DB_PASSWORD: A6tP3osxByeM
            SM_DB: mds
            SM_DB_HOST: mds-db
            SM_DB_PORT: 5432
        depends_on:
            - mds-db
        logging: *logging-service

    # Databases
    id-mapper-db:
        image: registry.gitlab.com/empaia/integration/ci-docker-images/custom-postgres:0.1.58@sha256:20ef7e9b4ef7e2b881db79f5ae5944a174c97ab6e13f4fbb679235b1e65eefa0
        shm_size: 1g
        restart: "no"
        environment:
            POSTGRES_DB: idm
            POSTGRES_USER: empaia_test
            POSTGRES_PASSWORD: A6tP3osxByeM
        volumes:
            - idm-db:/var/lib/postgresql/data:rw
        ports:
            - 127.0.0.1:10071:5432
        logging: *logging-service

    mds-db:
        image: registry.gitlab.com/empaia/integration/ci-docker-images/custom-postgres:0.1.58@sha256:20ef7e9b4ef7e2b881db79f5ae5944a174c97ab6e13f4fbb679235b1e65eefa0
        shm_size: 1g
        restart: "no"
        command: postgres -c config_file=/etc/postgresql/postgres.conf
        environment:
            POSTGRES_DB: mds
            POSTGRES_USER: empaia_test
            POSTGRES_PASSWORD: A6tP3osxByeM
        ports:
            - 127.0.0.1:10072:5432
        volumes:
            - mds-db:/var/lib/postgresql/data:rw
        logging: *logging-service

    job-execution-service-db:
        image: mongo:5.0.14@sha256:6cf671de7c85493d6dde9aafef383570161809336d6c41f431ac3a692155ece9
        restart: "no"
        volumes:
            - jes-db:/data/db:rw
        ports:
            - 127.0.0.1:10077:27017
        logging: *logging-service

    keycloak:
        image: registry.gitlab.com/empaia/integration/ci-docker-images/custom-keycloak:0.1.58@sha256:f2fd3d5d40c577de8714cb49ce9e207f2b98a6107483b9ece6b8813ddae5ec62
        restart: "no"
        environment:
            KEYCLOAK_USER: admin
            KEYCLOAK_PASSWORD: admin
        ports:
            - 127.0.0.1:10082:8080
        volumes:
            - keycloak-db:/opt/jboss/keycloak/standalone/data
        command:
            - "-b 0.0.0.0 -Dkeycloak.import=/opt/jboss/keycloak/imports/realm-export.json -Dkeycloak.profile.feature.upload_scripts=enabled"
        logging: *logging-service

volumes:
    idm-db:
        name: auth-idm-db
        external: false
    mds-db:
        name: auth-mds-db
        external: false
    jes-db:
        name: auth-jes-db
        external: false
    aaa-mock-vol:
        name: auth-aaa-mock-vol
        external: false
    mps-mock-vol:
        name: auth-mps-mock-vol
        external: false
    us-info:
        name: auth-us-info
        external: false
    keycloak-db:
        name: auth-keycloak-db
        external: false
    wbs-rsa-vol:
        name: auth-wbs-rsa-vol
        external: false
    es-rsa-vol:
        name: auth-es-rsa-vol
        external: false
    js-rsa-vol:
        name: auth-js-rsa-vol
        external: false
