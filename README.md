# Auto Uploader


## Format

One JSON File per Slide

```json
{
    "case_id": "Case-1", # use pseudonym here
    "id": "Slide-1", # use pseudonym here
    "path": "relative/path/to/mirax-main-file.mrxs",  # relative to ENV var AUPLD_WSI_DIR
    "tissue": "BREAST",  # see link full tag list below
    "stain": "H_AND_E"  # see link full tag list 
}
```

Example Tissue Tags:

```
["PROSTATE", "BREAST"]
```

Example Stain Tags:

```
["CEN17", "ER", "PR", "KI67", "HER2", "H_AND_E"]
```

* [Full Tag List](https://gitlab.com/empaia/integration/definitions/-/blob/main/tags/tags.csv)

## Repository Usage (dev)

start platform

```
cd auth
cp sample.env .env
docker-compose up -d
```

* DMC:  http://localhost:10054
* WBC1  http://localhost:10051
* WBC2  http://localhost:10055
* WBC3  http://localhost:10057

login:
1. user: pathologis, pw: secret
2. user: mta, pw: secret 


start auto-uploaded

```
cp auth/sample.env .env
poetry install
poetry run python3 -m auto_uploader
```

required envs for auto-uploader see bottom `.env`

## Usage (productive)

### Opt 1) via poetry

* skip starting dev services in `auth/docker-compose.yml`
* replace relevant ENV vars in .env for auto_uploader.

### Opt 2) via pip

* Goto https://gitlab.com/empaia/integration/auto-uploader/-/pipelines
* Download latest **build:artifacts**
* Navigate in terminal to downloaded **artifacts.zip**
* Create new Python3 venv (recommended, optional)
* Install via pip

```
python3 -m venv. venv
source .venv/bin/activate
unzip artifacts.zip
pip3 install dist/*.whl
# Set ENV VARS, e.g. via .env
python3 -m auto_uploader
```

### Opt 3) via Docker

- E.g. use the **docker-compose.yml** in this root dir as template
- Set ENV vars, e.g. directly in the **docker-compose.yml** file