# ########################################
# ## CODE SNIPPET FOR DEBUGGING PURPOSE ##
# ########################################

from auto_uploader import empaia_requests as er
from auto_uploader.singletons import logger

local_case_id = "IHC_HER2_230406a"


r = er.idm_get_local_case(local_case_id)
logger.info(f"GET IDM CASE [{local_case_id}]")
logger.info(f"Response content: {r.content}")
logger.info(f"Response status: {r.status_code}")

for idm_case in r.json()["items"]:
    empaia_case_id = idm_case["empaia_id"]

    r = er.mds_get_case(empaia_case_id, with_slides=True)
    logger.info(f"GET CDS empaia_case_id [{empaia_case_id}]")
    logger.info(f"Response content: {r.content}")
    logger.info(f"Response status: {r.status_code}")

    for slide in r.json()["slides"]:
        slide_id = slide["id"]

        r = er.mds_get_slide(slide_id)
        logger.info(f"GET CDS slide [{slide_id}]")
        logger.info(f"Response content: {r.content}")
        logger.info(f"Response status: {r.status_code}")

        r = er.mds_get_wsi_storage(slide_id)
        logger.info(f"GET SM slide [{slide_id}]")
        logger.info(f"Response content: {r.content}")
        logger.info(f"Response status: {r.status_code}")
