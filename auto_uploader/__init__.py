from importlib.metadata import version

__version__ = version("auto_uploader")
