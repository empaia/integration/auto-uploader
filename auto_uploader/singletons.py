import logging

from auto_uploader import __version__ as version
from auto_uploader import utils
from auto_uploader.login_manager import LoginManager
from auto_uploader.tus_auth_client import TusAuthClient

logging.info("Init logging")  # no logging according to level if not used once via "logging.xxx"

settings = utils.GeneralSettings()
settings.json_dir = utils.ensure_dir(settings.json_dir)
settings.wsi_dir = utils.ensure_dir(settings.wsi_dir)
settings.storage_file = utils.ensure_storage_file(settings.storage_file)

logger = logging.getLogger()
logger.setLevel(logging._nameToLevel[settings.log_level])
logger.info(f"Version: {version}")
logger.info("Auto-Uploader start with settings...")
for se in settings:
    logger.info(se)

login_manager = LoginManager(logger)

tus_client = TusAuthClient(
    tus_url=settings.us_host,
    chunk_size_b=settings.chunk_size_b,
    disable_send_filename=False,
    storage_file=settings.storage_file,
    login_manager=login_manager,
    logger=logger,
)
