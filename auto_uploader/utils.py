import json
from pathlib import Path

from pydantic import BaseSettings


class GetWsiFilesUnsupportedFormat(Exception):
    pass


class GetWsiFilesUnknownError(Exception):
    pass


class CreateCaseDuplicate(Exception):
    pass


class CreateCaseUnknownError(Exception):
    pass


class UploadWsiDuplicate(Exception):
    pass


class UploadWsiUnknowError(Exception):
    pass


class CreateSlideUnknownError(Exception):
    pass


def write_json_file(f_name: str, dictionary: dict):
    with Path(f_name).open(mode="w", encoding="utf8") as f:
        f.write(json.dumps(dictionary))


def read_json_file(f_name: str):
    with Path(f_name).open(mode="r", encoding="utf8") as f:
        dictionary = json.load(f)
        return dictionary


def ensure_dir(s: str) -> Path:
    p = Path(s).expanduser().resolve()
    p.mkdir(parents=True, exist_ok=True)
    assert p.exists()
    assert p.is_dir()
    return p


def ensure_storage_file(s: str) -> Path:
    p = Path(s).expanduser().resolve()
    p.parent.mkdir(parents=True, exist_ok=True)
    if p.exists():
        assert p.is_file()
    return p


def ensure_valid_file_path(path: str, sub_path: str):
    path = Path(path).resolve()
    sub_path = Path(sub_path).resolve()
    if not sub_path.exists():
        error = f"Given file path [{sub_path}] does not exist."
        raise ValueError(error)
    if not sub_path.is_file():
        error = f"Given file path [{sub_path}] not a file."
        raise ValueError(error)
    if path not in sub_path.parents:
        error = f"Given file path [{sub_path}] not in {[path]}."
        raise ValueError(error)
    return sub_path


class GeneralSettings(BaseSettings):
    us_host: str
    mds_host: str
    idm_host: str
    json_dir: str = "/opt/auto_uploader/JSONs"
    wsi_dir: str = "/opt/auto_uploader/WSIs"
    storage_file: str = "/opt/auto_uploader/storage_file.json"
    chunk_size_b: int = 1024000
    log_level: str = "WARN"
    scan_interval: int = 5
    annonymize_filenames = True

    class Config:
        env_file = ".env"
        env_prefix = "AUPLD_"
