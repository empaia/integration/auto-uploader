from glob import glob
from pathlib import Path

from auto_uploader.singletons import settings
from auto_uploader.utils import GetWsiFilesUnsupportedFormat, ensure_valid_file_path


def get_local_and_remote_files_paths(main_file: str, local_slide_id: str):
    local_main_file = str(Path(settings.wsi_dir).joinpath(Path(main_file)))
    ensure_valid_file_path(settings.wsi_dir, local_main_file)
    wsi_extension = Path(main_file).suffix
    # get file list approriate for format
    if wsi_extension.lower() == ".mrxs":
        return _get_mrxs(main_file, local_slide_id)
    else:
        raise GetWsiFilesUnsupportedFormat


def _get_mrxs(main_file: str, local_slide_id: str):
    local_files = []
    remote_files = []
    # main file relative to json
    remote_main_file = str(Path(main_file).name)
    # anonymize filename
    if settings.annonymize_filenames:
        remote_main_file = local_slide_id + ".mrxs"
    else:
        remote_main_file = str(Path(main_file).name)
    local_main_file = str(Path(settings.wsi_dir).joinpath(Path(main_file)))
    assert Path(local_main_file).is_file(), f"Specified file via path [{local_main_file}] does not exist."
    local_files.append(local_main_file)
    remote_files.append(remote_main_file)
    # sub files relative to json, within directory name equal mainfile
    mrxs_dir = Path(settings.wsi_dir).joinpath(Path(local_main_file).stem)
    assert Path(mrxs_dir).is_dir(), f"Directory [{mrxs_dir}] does not exist."
    for local_sub_file in glob(f"{mrxs_dir}/*"):
        # anonymize filename
        if settings.annonymize_filenames:
            remote_sub_file = str(Path(local_slide_id).joinpath(Path(local_sub_file).name))
        else:
            remote_sub_file = str(Path((Path(local_main_file).stem)).joinpath(Path(local_sub_file).name))
        local_files.append(local_sub_file)
        remote_files.append(remote_sub_file)
    return local_files, remote_files
