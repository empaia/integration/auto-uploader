import requests

from auto_uploader.singletons import login_manager, settings


def mds_post_case(creator_id, description):
    headers = login_manager.mta_user()
    url = f"{settings.mds_host}/private/v3/cases"
    data = {
        "creator_id": creator_id,
        "creator_type": "USER",
        "description": description,
    }
    r = requests.post(url, json=data, headers=headers)
    return r


def mds_get_case(case_id, with_slides=False):
    headers = login_manager.mta_user()
    url = f"{settings.mds_host}/v3/cases/{case_id}"
    params = {}
    if with_slides:
        params["with_slides"] = True
    r = requests.get(url, headers=headers, params=params)
    return r


def mds_get_cases(skip, limit):
    headers = login_manager.mta_user()
    url = f"{settings.mds_host}/v3/cases"
    params = {
        "skip": skip,
        "limit": limit,
    }
    r = requests.get(url, params=params, headers=headers)
    return r


def mds_post_slide(case_id, tissue, stain, block=None):
    headers = login_manager.mta_user()
    url = f"{settings.mds_host}/private/v3/slides"
    data = {
        "case_id": case_id,
        "tissue": tissue,
        "stain": stain,
    }
    if block:
        data["block"] = block
    r = requests.post(url, json=data, headers=headers)
    return r


def mds_get_slide(slide_id):
    headers = login_manager.mta_user()
    url = f"{settings.mds_host}/v3/slides/{slide_id}"
    r = requests.get(url, headers=headers)
    return r


def mds_delete_case(case_id):
    headers = login_manager.mta_user()
    url = f"{settings.mds_host}/v3/cases/{case_id}"
    payload = {"deleted": True}
    r = requests.put(url, headers=headers, json=payload)
    return r


def mds_delete_slide(slide_id):
    headers = login_manager.mta_user()
    url = f"{settings.mds_host}/v3/slides/{slide_id}"
    payload = {"deleted": True}
    r = requests.put(url, headers=headers, json=payload)
    return r


def mds_register_wsi(slide_id, storage_adresses, storage_adresses_ids):
    headers = login_manager.mta_user()
    data = {
        "slide_id": slide_id,
        "storage_type": "fs",
        "storage_addresses": [
            {
                "address": str(storage_adresses[0]),
                "main_address": True,
                "storage_address_id": str(storage_adresses_ids[0]),
                "slide_id": slide_id,
            }
        ],
    }
    for sa, sa_id in zip(storage_adresses[1:], storage_adresses_ids[1:]):
        data["storage_addresses"].append(
            {
                "address": str(sa),
                "main_address": False,
                "storage_address_id": str(sa_id),
                "slide_id": slide_id,
            }
        )
    url = f"{settings.mds_host}/private/v3/slides/storage"
    r = requests.post(url, json=data, headers=headers)
    return r


def mds_get_wsi_storage(slide_id):
    headers = login_manager.mta_user()
    url = f"{settings.mds_host}/private/v3/slides/{slide_id}/storage"
    r = requests.get(url, headers=headers)
    return r


def mds_delete_slide_storage(slide_id):
    headers = login_manager.mta_user()
    url = f"{settings.mds_host}/private/v3/slides/{slide_id}/storage"
    r = requests.delete(url, headers=headers)
    return r


def idm_post_case(mds_url, empaia_id, local_id):
    headers = login_manager.mta_user()
    url = f"{settings.idm_host}/v1/cases"
    data = {"mds_url": mds_url, "empaia_id": empaia_id, "local_id": local_id}
    r = requests.post(url, json=data, headers=headers)
    return r


def idm_get_local_case(local_id):
    headers = login_manager.mta_user()
    url = f"{settings.idm_host}/v1/cases/local/{local_id}"
    r = requests.get(url, headers=headers)
    return r


def idm_get_local_slide(local_id):
    headers = login_manager.mta_user()
    url = f"{settings.idm_host}/v1/slides/local/{local_id}"
    r = requests.get(url, headers=headers)
    return r


def idm_post_slide(mds_url, empaia_id, local_id):
    headers = login_manager.mta_user()
    url = f"{settings.idm_host}/v1/slides"
    data = {"mds_url": mds_url, "empaia_id": empaia_id, "local_id": local_id}
    r = requests.post(url, json=data, headers=headers)
    return r


def idm_delete_slide(empaia_id):
    headers = login_manager.mta_user()
    url = f"{settings.idm_host}/v1/slides/empaia/{empaia_id}"
    r = requests.delete(url, headers=headers)
    return r
