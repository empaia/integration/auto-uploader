import traceback
from glob import glob
from pathlib import Path
from time import sleep

from tusclient.exceptions import TusCommunicationError

from auto_uploader import empaia_requests as er
from auto_uploader.singletons import logger, login_manager, settings, tus_client
from auto_uploader.utils import (
    CreateCaseDuplicate,
    CreateCaseUnknownError,
    CreateSlideUnknownError,
    GetWsiFilesUnknownError,
    GetWsiFilesUnsupportedFormat,
    UploadWsiDuplicate,
    UploadWsiUnknowError,
    read_json_file,
    write_json_file,
)
from auto_uploader.wsi_formats import get_local_and_remote_files_paths


def create_case_or_get_existing(local_case_id: str, creator_id: str):
    r = er.idm_get_local_case(local_case_id)
    # GET IDM case
    logger.info(f"GET IDM case [{local_case_id}]")
    logger.info(f"Response content: {r.content}")
    logger.info(f"Response status: {r.status_code}")
    empaia_case_ids_not_deleted = []
    # at least 1 entry at id-mapper-service
    if r.status_code == 200:
        idm_cases = r.json()
        for idm_case in idm_cases["items"]:
            empaia_case_id = idm_case["empaia_id"]
            # GET CDS case
            logger.info(f"GET CDS case empaia_case_id [{empaia_case_id}]")
            r = er.mds_get_case(empaia_case_id)
            r.raise_for_status()
            empaia_case = r.json()
            # check deleted
            if empaia_case["deleted"] is False or empaia_case["deleted"] is None:
                logger.info(f"CDS case empaia_case_id marked NOT as deleted [{empaia_case_id}]")
                empaia_case_ids_not_deleted.append(empaia_case_id)
            else:
                logger.info(f"CDS case empaia_case_id marked as deleted [{empaia_case_id}]")
    # no entry at id-mapper-service, create new
    if len(empaia_case_ids_not_deleted) == 0:
        logger.info(f"Creating new case [{local_case_id}]")
        # POST CDS case
        logger.info(f"POST CDS case [{local_case_id}]")
        r = er.mds_post_case(creator_id, ".")
        r.raise_for_status()
        empaia_case_id = r.json()["id"]
        # POST IDM case
        logger.info(f"POST IDM case [{local_case_id}]")
        r = er.idm_post_case(settings.mds_host, empaia_case_id, local_case_id)
        r.raise_for_status()
    # exactly 1 entry at id-mapper-service
    elif len(empaia_case_ids_not_deleted) == 1:
        empaia_case_id = empaia_case_ids_not_deleted[0]
        logger.info(f"Case already exists empaia_case_id/local_case_id [{empaia_case_id},{local_case_id}]:")
        logger.info(f"Response content: {r.content}")
        logger.info(f"Response status: {r.status_code}")
    # >= 2 entry at id-mapper, non of it deleted, we have a problem
    else:
        raise CreateCaseDuplicate
    return empaia_case_id


def create_slide(
    empaia_case_id: str,
    local_slide_id: str,
    remote_files: list,
    upload_ids: list,
    tissue: str,
    stain: str,
):
    logger.info(f"Creating new slide [{local_slide_id}]")
    # POST CDS slide
    logger.info(f"POST CDS slide [{local_slide_id}]")
    r = er.mds_post_slide(empaia_case_id, tissue, stain)
    r.raise_for_status()
    empaia_slide_id = r.json()["id"]
    # POST STORAGE
    logger.info(f"POST STORAGE slide [{local_slide_id}]")
    r = er.mds_register_wsi(empaia_slide_id, remote_files, upload_ids)
    r.raise_for_status()
    # POST IDM slide
    logger.info(f"POST IDM slide [{local_slide_id}]")
    r = er.idm_post_slide(settings.mds_host, empaia_slide_id, local_slide_id)
    r.raise_for_status()
    return empaia_slide_id


def process_wsi(
    wsi_specs: dict,
):
    headers = login_manager.mta_user()
    creator_id = headers["user-id"]

    local_case_id = wsi_specs["case_id"]
    local_slide_id = wsi_specs["id"]
    main_file = wsi_specs["path"]
    tissue = wsi_specs["tissue"]
    stain = wsi_specs["stain"]

    # get files list
    try:
        local_files, remote_files = get_local_and_remote_files_paths(main_file, local_slide_id)
    except GetWsiFilesUnsupportedFormat as e:
        raise e from None
    except Exception as e:
        raise GetWsiFilesUnknownError from e

    # create case if not exists or get existing id
    try:
        empaia_case_id = create_case_or_get_existing(local_case_id, creator_id)
    except CreateCaseDuplicate as e:
        raise e from None
    except Exception as e:
        raise CreateCaseUnknownError from e

    # add case_id to remote_filenames
    for i in range(len(remote_files)):
        remote_files[i] = empaia_case_id + "/" + remote_files[i]

    # upload files
    try:
        upload_ids = []
        for i in range(len(local_files)):
            upload_id = tus_client.upload(file_path=local_files[i], file_name_meta=remote_files[i])
            upload_ids.append(upload_id)
    except TusCommunicationError as e:
        if e.status_code == 409 and "file_name already exists" in str(e.response_content):
            raise UploadWsiDuplicate from e
        else:
            raise UploadWsiUnknowError from e
    except Exception as e:
        raise UploadWsiUnknowError from e

    # create slide
    try:
        _empaia_slide_id = create_slide(
            empaia_case_id,
            local_slide_id,
            remote_files,
            upload_ids,
            tissue,
            stain,
        )
    except Exception as e:
        raise CreateSlideUnknownError from e


def clear_sorage_file():
    with Path(settings.storage_file).open(mode="w", encoding="utf8") as f:
        f.write("")


def main():
    while True:
        for f_name in glob(f"{settings.json_dir}/*.json"):
            try:
                clear_sorage_file()
                wsi_specs = read_json_file(f_name)

                # already uploaded, being uploaded, or failed
                if "status" in wsi_specs:
                    continue

                try:
                    logger.info(f"Mark file as PROCESSING [{f_name}]")
                    wsi_specs["status"] = "PROCESSING"
                    write_json_file(f_name, wsi_specs)

                    # main stuff happening here
                    process_wsi(wsi_specs)

                    logger.info(f"Mark file as FINISHED [{f_name}]")
                    wsi_specs["status"] = "FINISHED"
                    write_json_file(f_name, wsi_specs)

                # error handling / logging
                except GetWsiFilesUnsupportedFormat:
                    logger.info(f"Mark file as GetWsiFilesUnsupportedFormat [{f_name}]")
                    wsi_specs["status"] = "FAILED: GetWsiFilesUnsupportedFormat"
                    wsi_specs["traceback"] = traceback.format_exc()
                    write_json_file(f_name, wsi_specs)
                except GetWsiFilesUnknownError:
                    logger.info(f"Mark file as GetWsiFilesUnknownError [{f_name}]")
                    wsi_specs["status"] = "FAILED: GetWsiFilesUnknownError"
                    wsi_specs["traceback"] = traceback.format_exc()
                    write_json_file(f_name, wsi_specs)
                except CreateCaseDuplicate:
                    logger.info(f"Mark file as CreateCaseDuplicate [{f_name}]")
                    wsi_specs["status"] = "FAILED: CreateCaseDuplicate"
                    wsi_specs["traceback"] = traceback.format_exc()
                    write_json_file(f_name, wsi_specs)
                except CreateCaseUnknownError:
                    logger.info(f"Mark file as CreateCaseUnknownError [{f_name}]")
                    wsi_specs["status"] = "FAILED: CreateCaseUnknownError"
                    wsi_specs["traceback"] = traceback.format_exc()
                    write_json_file(f_name, wsi_specs)
                except UploadWsiDuplicate:
                    logger.info(f"Mark file as UploadWsiDuplicate [{f_name}]")
                    wsi_specs["status"] = "FAILED: UploadWsiDuplicate"
                    wsi_specs["traceback"] = traceback.format_exc()
                    write_json_file(f_name, wsi_specs)
                except UploadWsiUnknowError:
                    logger.info(f"Mark file as UploadWsiUnknowError [{f_name}]")
                    wsi_specs["status"] = "FAILED: UploadWsiUnknowError"
                    wsi_specs["traceback"] = traceback.format_exc()
                    write_json_file(f_name, wsi_specs)
                except CreateSlideUnknownError:
                    logger.info(f"Mark file as CreateSlideUnknownError [{f_name}]")
                    wsi_specs["status"] = "FAILED: CreateSlideUnknownError"
                    wsi_specs["traceback"] = traceback.format_exc()
                    write_json_file(f_name, wsi_specs)
                except Exception:
                    logger.info(f"Mark file as Exception [{f_name}]")
                    wsi_specs["status"] = "FAILED: Exception"
                    wsi_specs["traceback"] = traceback.format_exc()
                    write_json_file(f_name, wsi_specs)
            except Exception as e:
                # most likely error opening the json file
                # will be retried
                logger.exception(f"Exception opening file, will be retried [{f_name}]")
                logger.exception(e)

        sleep(settings.scan_interval)


if __name__ == "__main__":
    main()
