import uuid
from logging import Logger
from pathlib import Path

from pydantic import AnyHttpUrl
from tusclient.client import TusClient
from tusclient.exceptions import TusCommunicationError
from tusclient.storage.filestorage import FileStorage

from auto_uploader.login_manager import LoginManager


class TusAuthClient:
    def __init__(
        self,
        tus_url: AnyHttpUrl,
        login_manager: LoginManager,
        logger: Logger,
        disable_send_filename: bool,
        chunk_size_b: int,
        storage_file: str,
    ):
        self.tus_client = TusClient(tus_url.rstrip("/") + "/v1/files")
        self.login_manager = login_manager
        self.logger = logger
        self.chunk_size_b = chunk_size_b
        self.storage_file = FileStorage(storage_file)

    def upload(self, file_path: str, file_name_meta: str):
        file_path = Path(file_path).expanduser()
        metadata = {}
        metadata["file_name"] = file_name_meta
        metadata["id"] = str(uuid.uuid4())

        retry = True
        resumed = False

        while retry:
            try:
                retry = False
                headers = self.login_manager.mta_user()
                self.tus_client.set_headers(headers)
                uploader = self.tus_client.uploader(
                    file_path=file_path,
                    chunk_size=self.chunk_size_b,
                    store_url=True,
                    url_storage=self.storage_file,
                    metadata=metadata,
                    retries=5,
                    retry_delay=10,
                )
                if not resumed:
                    self.logger.info(f"Start upload of file [{file_path}]")
                else:
                    self.logger.info(f"Resuming upload of file [{file_path}]")
                uploader.upload()
            except TusCommunicationError as e:
                if e.status_code == 401:
                    retry = True
                    resumed = True
                    self.logger.debug(f"Token expired, getting new token and resume upload of file {file_path}")
                else:
                    self.logger.exception(f"Upload failed of file [{file_path}]")
                    self.logger.exception(e.status_code, e.response_content)
                    raise e from None
            except Exception as e:
                self.logger.exception(f"Upload failed of file [{file_path}]")
                self.logger.exception(e)
                raise e from None

        self.logger.info(f"Finished upload of file [{file_path}]")
        return metadata["id"]
